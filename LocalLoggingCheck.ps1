#Requires -RunAsAdministrator

param (
    [switch]$ExportHtml
)

# Import required modules
Import-Module GroupPolicy -ErrorAction SilentlyContinue

# Define CIS Benchmark recommendations for logging-related settings
$cisBenchmarks = @{
    "Audit Credential Validation" = "Success and Failure"
    "Audit Kerberos Authentication Service" = "Success and Failure"
    "Audit Kerberos Service Ticket Operations" = "Success and Failure"
    "Audit Computer Account Management" = "Success and Failure"
    "Audit Other Account Management Events" = "Success and Failure"
    "Audit Security Group Management" = "Success and Failure"
    "Audit User Account Management" = "Success and Failure"
    "Audit Process Creation" = "Success"
    "Audit Process Termination" = "Success"
    "Audit Account Lockout" = "Success"
    "Audit Logoff" = "Success"
    "Audit Logon" = "Success and Failure"
    "Audit Other Logon/Logoff Events" = "Success and Failure"
    "Audit Special Logon" = "Success"
    "Audit File System" = "Success and Failure"
    "Audit Registry" = "Success and Failure"
    "Audit Audit Policy Change" = "Success and Failure"
    "Audit Authentication Policy Change" = "Success"
    "Audit Authorization Policy Change" = "Success"
    "Audit Sensitive Privilege Use" = "Success and Failure"
    "Audit IPsec Driver" = "Success and Failure"
    "Audit Other System Events" = "Success and Failure"
    "Audit Security State Change" = "Success"
    "Audit Security System Extension" = "Success"
    "Audit System Integrity" = "Success and Failure"
    "Maximum Application Log Size" = 1024000
    "Retention Method Application Log" = "Overwrite events as needed"
    "Audit Application Log Max Size Behavior" = "Disabled"
    "Maximum Security Log Size" = 1024000
    "Retention Method Security Log" = "Overwrite events as needed"
    "Audit Security Log Max Size Behavior" = "Disabled"
    "Maximum System Log Size" = 1024000
    "Retention Method System Log" = "Overwrite events as needed"
    "Audit System Log Max Size Behavior" = "Disabled"
    "Maximum PowerShell Log Size" = 1024000
    "Turn on PowerShell Script Block Logging" = "Enabled"
    "Turn on PowerShell Transcription" = "Enabled"
    "Process Creation Command Line Logging" = "Enabled"
    "Sysmon Installed and Configured" = "Enabled"
}

# Function to get current audit policy settings
function Get-AuditPolicySettings {
    try {
        $auditSettings = auditpol /get /category:* /r | ConvertFrom-Csv -ErrorAction Stop
        $currentSettings = @{}
        
        foreach ($policy in $auditSettings) {
            $policyName = $policy.Subcategory
            $success = $policy.'Success Auditing'
            $failure = $policy.'Failure Auditing'
            $setting = "$success and $failure"
            if ($setting -eq "No Auditing and No Auditing") {
                $setting = "No Auditing"
            }
            $currentSettings[$policyName] = $setting
        }
        $cmdLineReg = Get-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System\Audit" -Name "ProcessCreationIncludeCmdLine_Enabled" -ErrorAction SilentlyContinue
        $currentSettings["Process Creation Command Line Logging"] = if ($cmdLineReg -and $cmdLineReg.ProcessCreationIncludeCmdLine_Enabled -eq 1) { "Enabled" } else { "Disabled" }
        return $currentSettings
    } catch {
        Write-Warning "Failed to retrieve audit policy settings: $_"
        return @{}
    }
}

# Function to get event log settings
function Get-EventLogSettings {
    $logSettings = @{}
    $logs = @("Security", "Application", "System", "Windows PowerShell")
    
    foreach ($log in $logs) {
        try {
            $logInfo = Get-ItemProperty -Path "HKLM:\SYSTEM\CurrentControlSet\Services\EventLog\$log" -ErrorAction Stop
            $maxSize = if ($logInfo.MaxSize) { [int]($logInfo.MaxSize / 1024) } else { 0 }
            $retention = if ($logInfo.Retention -eq 0 -or $logInfo.AutoBackupLogFiles -eq 1) { "Overwrite events as needed" } else { "Do not overwrite events" }
            $restrict = if ($logInfo.RestrictGuestAccess -eq 1) { "Disabled" } else { "Enabled" }
            
            if ($log -eq "Windows PowerShell") {
                $logSettings["Maximum PowerShell Log Size"] = $maxSize
            } else {
                $logSettings["Maximum $log Log Size"] = $maxSize
                $logSettings["Retention Method $log Log"] = $retention
                $logSettings["Audit $log Log Max Size Behavior"] = $restrict
            }
        } catch {
            Write-Warning "Failed to retrieve $log log settings: $_"
            if ($log -eq "Windows PowerShell") {
                $logSettings["Maximum PowerShell Log Size"] = "Not Configured"
            } else {
                $logSettings["Maximum $log Log Size"] = "Not Configured"
                $logSettings["Retention Method $log Log"] = "Not Configured"
                $logSettings["Audit $log Log Max Size Behavior"] = "Not Configured"
            }
        }
    }

    try {
        $psSettings = Get-ItemProperty -Path "HKLM:\SOFTWARE\Policies\Microsoft\Windows\PowerShell" -ErrorAction SilentlyContinue
        $logSettings["Turn on PowerShell Script Block Logging"] = if ($psSettings -and $psSettings.EnableScriptBlockLogging -eq 1) { "Enabled" } else { "Disabled" }
        $logSettings["Turn on PowerShell Transcription"] = if ($psSettings -and $psSettings.EnableTranscripting -eq 1) { "Enabled" } else { "Disabled" }
    } catch {
        Write-Warning "Failed to retrieve PowerShell logging settings: $_"
        $logSettings["Turn on PowerShell Script Block Logging"] = "Not Configured"
        $logSettings["Turn on PowerShell Transcription"] = "Not Configured"
    }

    try {
        $sysmonService = Get-Service -Name "Sysmon*" -ErrorAction Stop
        $sysmonConfig = Get-ItemProperty -Path "HKLM:\SYSTEM\CurrentControlSet\Services\Sysmon*" -ErrorAction SilentlyContinue
        $logSettings["Sysmon Installed and Configured"] = if ($sysmonService -and $sysmonConfig) { "Enabled" } else { "Disabled" }
    } catch {
        Write-Warning "Failed to check Sysmon status: $_"
        $logSettings["Sysmon Installed and Configured"] = "Not Configured"
    }

    return $logSettings
}

# Get current settings
$auditSettings = Get-AuditPolicySettings
$logSettings = Get-EventLogSettings
$allSettings = $auditSettings + $logSettings

# Create results array
$results = @()

foreach ($benchmark in $cisBenchmarks.GetEnumerator()) {
    $settingName = $benchmark.Name
    $recommended = $benchmark.Value
    $current = $allSettings[$settingName]
    
    if ($null -eq $current) {
        $current = "Not Configured"
    }
    
    $isCompliant = $false
    if ($settingName -like "*Size*") {
        try {
            $isCompliant = [int]$current -ge [int]$recommended
        } catch {
            $isCompliant = $current -eq $recommended
        }
    } else {
        $isCompliant = $current -eq $recommended
    }
    
    $status = if ($isCompliant) { "Pass" } else { "Fail" }
    
    $results += [PSCustomObject]@{
        "Setting" = $settingName
        "CIS Recommended" = $recommended
        "Current Setting" = $current
        "Status" = $status
    }
}

# Define groups for both console and HTML
$groups = @{
    "Maximum Log Sizes" = $results | Where-Object { $_.Setting -like "*Maximum*Size*" }
    "Retention Methods" = $results | Where-Object { $_.Setting -like "*Retention*" }
    "Audit Policies" = $results | Where-Object { $_.Setting -like "Audit*" -and $_.Setting -notlike "*Size*" }
    "PowerShell Logging" = $results | Where-Object { $_.Setting -in "Turn on PowerShell Script Block Logging", "Turn on PowerShell Transcription" }
    "Process Creation Logging" = $results | Where-Object { $_.Setting -eq "Process Creation Command Line Logging" }
    "Sysmon Configuration" = $results | Where-Object { $_.Setting -like "*Sysmon*" }
    "Other Settings" = $results | Where-Object { $_.Setting -notlike "*Maximum*Size*" -and $_.Setting -notlike "*Retention*" -and $_.Setting -notlike "Audit*" -and $_.Setting -notin "Turn on PowerShell Script Block Logging", "Turn on PowerShell Transcription" -and $_.Setting -ne "Process Creation Command Line Logging" -and $_.Setting -notlike "*Sysmon*" }
}

# Console output with Pass/Fail
foreach ($group in $groups.GetEnumerator()) {
    if ($group.Value) {
        Write-Host "`n--- $($group.Name) ---" -ForegroundColor Cyan
        Write-Host ("{0,-40} {1,-25} {2,-25}" -f "Setting", "CIS Recommended", "Current Setting") -ForegroundColor Yellow
        Write-Host ("-" * 40 + " " + "-" * 25 + " " + "-" * 25) -ForegroundColor Gray
        
        foreach ($item in $group.Value) {
            $color = if ($item.Status -eq "Pass") { "Green" } else { "Red" }
            Write-Host ("{0,-40} {1,-25} {2,-25}" -f $item.Setting, $item."CIS Recommended", $item."Current Setting") -NoNewline
            Write-Host " [$($item.Status)]" -ForegroundColor $color
        }
    }
}

# Optional HTML export with Bootstrap, collapsible tables, auto-open, pass/fail counts, and pie chart
if ($ExportHtml) {
    $systemName = $env:COMPUTERNAME
    $reportTime = Get-Date -Format "yyyy-MM-dd HH:mm:ss"
    
    # Calculate Pass and Fail counts
    $passCount = ($results | Where-Object { $_.Status -eq "Pass" }).Count
    $failCount = ($results | Where-Object { $_.Status -eq "Fail" }).Count
    
    $html = @"
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Local Logging Settings vs CIS Benchmarks</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/chart.js@4.4.0/dist/chart.umd.min.js"></script>
    <style>
        body { padding: 20px; background-color: #f8f9fa; }
        .header { margin-bottom: 20px; }
        .group-header { background-color: #007bff; color: white; padding: 10px; margin-top: 20px; cursor: pointer; }
        .table { margin-bottom: 30px; }
        .table th { background-color: #343a40; color: white; }
        .table td.status-pass { background-color: #d4edda; text-align: center; color: green !important; }
        .table td.status-fail { background-color: #f8d7da; text-align: center; color: red !important; }
        .chart-container { width: 200px; height: 200px; margin: 0 auto 20px; }
    </style>
</head>
<body>
    <div class="container">
        <div class="header">
            <h1 class="text-primary">Local Logging Settings vs CIS Benchmarks</h1>
            <p class="lead">System: $systemName</p>
            <p class="lead">Report Generated: $reportTime</p>
            <p class="lead">Summary: <span class="text-success">$passCount Pass</span> | <span class="text-danger">$failCount Fail</span></p>
            <div class="chart-container">
                <canvas id="summaryChart"></canvas>
            </div>
        </div>
"@

    $collapseId = 0
    foreach ($group in $groups.GetEnumerator()) {
        if ($group.Value) {
            $collapseId++
            $html += @"
        <h3 class="group-header" data-bs-toggle="collapse" data-bs-target="#collapse$collapseId" aria-expanded="true" aria-controls="collapse$collapseId">$($group.Name)</h3>
        <div id="collapse$collapseId" class="collapse show">
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th scope="col">Setting</th>
                        <th scope="col">CIS Recommended</th>
                        <th scope="col">Current Setting</th>
                        <th scope="col">Status</th>
                    </tr>
                </thead>
                <tbody>
"@
            foreach ($item in $group.Value) {
                $statusClass = if ($item.Status -eq "Pass") { "status-pass" } else { "status-fail" }
                $html += @"
                <tr>
                    <td>$($item.Setting)</td>
                    <td>$($item.'CIS Recommended')</td>
                    <td>$($item.'Current Setting')</td>
                    <td class="$statusClass">$($item.Status)</td>
                </tr>
"@
            }
            $html += @"
                </tbody>
            </table>
        </div>
"@
        }
    }

    $html += @"
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
    <script>
        const ctx = document.getElementById('summaryChart').getContext('2d');
        const summaryChart = new Chart(ctx, {
            type: 'pie',
            data: {
                labels: ['Pass', 'Fail'],
                datasets: [{
                    data: [$passCount, $failCount],
                    backgroundColor: ['#d4edda', '#f8d7da'],
                    borderColor: ['#28a745', '#dc3545'],
                    borderWidth: 1
                }]
            },
            options: {
                responsive: true,
                maintainAspectRatio: true,
                plugins: {
                    legend: {
                        position: 'bottom'
                    }
                }
            }
        });
    </script>
</body>
</html>
"@
    try {
        $html | Out-File "GPO_Audit_Report.html" -ErrorAction Stop
        Write-Host "`nReport exported to GPO_Audit_Report.html"
        Start-Process "GPO_Audit_Report.html"
    } catch {
        Write-Warning "Failed to export or open HTML report: $_"
    }
}