@echo off
Echo Ediiting the registry to increase the size of specific logs
Echo ===========================================================
Echo Setting Security log max size to 524288000
reg add hklm\system\CurrentControlSet\services\eventlog\Security /v MaxSize /t REG_DWORD /d 524288000 /f
Echo Security log modification complete
Echo Setting Windows PowerShell log max size to 262144000
reg add "hklm\system\CurrentControlSet\services\eventlog\Windows PowerShell" /v MaxSize /t REG_DWORD /d 262144000 /f
Echo Windows PowerShell log modification complete
Echo Setting System log max size to 262144000
reg add hklm\system\CurrentControlSet\services\eventlog\System /v MaxSize /t REG_DWORD /d 262144000 /f
Echo Windows System log modification complete
Echo Setting Application log max size to 262144000
reg add hklm\system\CurrentControlSet\services\eventlog\Application /v MaxSize /t REG_DWORD /d 262144000 /f
Echo Application log modification complete
Echo   
Echo Completed Windows log resizing
Echo   
Echo Initializing additional DNS logging
Echo ===========================================================
wevtutil sl Microsoft-Windows-DNS-Client/Operational /e:true
Echo Enabled Microsoft-Windows-DNS-Client/Operational log
Echo   
Echo Enableing PowerShell advanced logging, ProcessCreationIncludeCmdLin, and additional advanced auditing items
Echo ===========================================================
reg add "hklm\software\microsoft\windows\currentversion\policies\system\audit" /v ProcessCreationIncludeCmdLine_Enabled /t REG_DWORD /d 1
reg add "hklm\System\CurrentControlSet\Control\Lsa" /v SCENoApplyLegacyAuditPolicy /t REG_DWORD /d 1
reg add "hklm\Software\Policies\Microsoft\Windows\PowerShell\ModuleLogging" /v EnableModuleLogging /t REG_DWORD /d 1
reg add "hklm\Software\Policies\Microsoft\Windows\PowerShell\ScriptBlockLogging" /v EnableScriptBlockLogging /t REG_DWORD /d 1
Echo   
Echo Setting audit policy to meet CIS standards with some additional logging points
Echo ===========================================================
Echo Setting Account Logons audit policy
auditpol /set /subcategory:"Credential Validation" /success:enable /failure:enable
auditpol /set /subcategory:"Other Account Logon Events" /success:enable /failure:enable
Echo "Account Logons policy is now set"
Echo " "
Echo "Setting Account Management audit policy"
auditpol /set /subcategory:"Application Group Management" /success:enable /failure:enable
auditpol /set /subcategory:"Computer Account Management" /success:enable /failure:enable
auditpol /set /subcategory:"Distribution Group Management" /success:enable /failure:enable
auditpol /set /subcategory:"Other Account Management Events" /success:enable /failure:enable
auditpol /set /subcategory:"Security Group Management" /success:enable /failure:enable
auditpol /set /subcategory:"User Account Management" /success:enable /failure:enable
Echo "Account Management policy is now set"
Echo " "
Echo "Setting Directory Service audit policy"
auditpol /set /subcategory:"Directory Service Changes" /success:enable /failure:enable
Echo "Directory Service Changes audit policy is now set"
Echo " "
Echo "Setting Detailed Tracking audit policy"
auditpol /set /subcategory:"Plug and Play Events" /success:enable
auditpol /set /subcategory:"Process Creation" /success:enable /failure:enable
auditpol /set /subcategory:"Process Termination" /success:enable /failure:enable
auditpol /set /subcategory:"RPC Events" /success:enable /failure:enable
auditpol /set /subcategory:"Token Right Adjusted Events" /success:enable
Echo "Detailed Tracking audit policy is now set"
Echo " "
Echo "Setting Logon and Logoff audit policy"
auditpol /set /subcategory:"Group Membership" /success:enable
auditpol /set /subcategory:"Logon" /failure:enable
auditpol /set /subcategory:"Network Policy Server" /success:enable /failure:enable
auditpol /set /subcategory:"Other Logon/Logoff Events" /success:enable /failure:enable
auditpol /set /subcategory:"Special Logon" /success:enable /failure:enable
Echo "Logon and Logoff audit policy is now set"
Echo " "
Echo "Setting Object Access audit policy"
auditpol /set /subcategory:"Application Generated" /success:enable /failure:enable
auditpol /set /subcategory:"Certification Services" /success:enable /failure:enable
auditpol /set /subcategory:"Detailed File Share" /success:enable
auditpol /set /subcategory:"File Share" /success:enable /failure:enable
auditpol /set /subcategory:"File System" /success:enable
auditpol /set /subcategory:"Filtering Platform Connection" /success:enable
auditpol /set /subcategory:"Kernel Object" /success:enable /failure:enable
auditpol /set /subcategory:"Registry" /success:enable
auditpol /set /subcategory:"Removable Storage" /success:enable /failure:enable
Echo "Logon and Logoff policy is now set"
Echo " "
Echo "Setting Policy Change audit policy"
auditpol /set /subcategory:"Audit Policy Change" /success:enable /failure:enable
auditpol /set /subcategory:"Authentication Policy Change" /success:enable /failure:enable
auditpol /set /subcategory:"Authorization Policy Change" /success:enable /failure:enable
auditpol /set /subcategory:"Filtering Platform Policy Change" /success:enable
Echo "Policy Change audit policy is now set"
Echo " "
Echo "Setting Privilege Use audit policy"
auditpol /set /subcategory:"Sensitive Privilege Use" /success:enable /failure:enable
Echo "Privilege Use audit policy is now set"
Echo " "
Echo "Setting System audit policy"
auditpol /set /subcategory:"IPsec Driver" /success:enable
auditpol /set /subcategory:"Other System Events" /failure:enable
auditpol /set /subcategory:"Security State Change" /success:enable /failure:enable
auditpol /set /subcategory:"Security System Extension" /success:enable /failure:enable
auditpol /set /subcategory:"System Integrity" /success:enable /failure:enable
Echo "System audit policy is now set"